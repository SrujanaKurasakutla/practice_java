package com.example.onetomany.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.onetomany.model.Employee;

public interface EmployeeDao extends JpaRepository<Employee, Integer>{

}
