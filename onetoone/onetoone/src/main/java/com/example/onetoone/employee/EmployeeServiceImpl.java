package com.example.onetoone.employee;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.onetoone.address.Address;
import com.example.onetoone.address.AddressDao;


@Service
public class EmployeeServiceImpl implements EmployeeService{
	@Autowired
    EmployeeDao employeeDao;
    @Autowired
    AddressDao addressDao;
	@Override
	public List<Employee> getAllEmployees() {
		return employeeDao.findAll();
	}

	@Override
	public Employee addEmployee(Employee employee) {
		//Address address = new Address();
		/*Address address = addressDao.findById(employee.getAddress().getId()).orElse(null);
        if (null == address) {
            address = new Address();
        }*/
        //address.setEmployee(employee);
        //employee.setAddress(employee.getAddress());
          
        return employeeDao.save(employee);
	}

	@Override
	public Employee editEmployees(Employee employee) {
		Optional<Employee> emp = employeeDao.findByFirstName(employee.getFirstName());
		if(emp != null) {
			return employeeDao.save(employee);
		}else {
			return null;
		}
		
	}

	@Override
	public void deleteEmployees(Integer employeeId) {
		employeeDao.deleteById(employeeId);
		
	}

}
