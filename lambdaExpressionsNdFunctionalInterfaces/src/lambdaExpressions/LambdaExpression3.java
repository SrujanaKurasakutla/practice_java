package lambdaExpressions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import products.Product;

public class LambdaExpression3 {
	public static void main(String[] args) {
		List<Product> products = new ArrayList<>();
		products.add(new Product(1, "table", 2000f));
		products.add(new Product(2, "chair", 1200f));
		products.add(new Product(3, "bench", 4500f));
		products.add(new Product(4, "mouse", 800f));
		products.add(new Product(5, "box", 790f));

		Stream<Product> filtered_products = products.stream().filter(product -> product.getPrice() > 1000);

		filtered_products.forEach(product -> System.out.println(product.getName() + ": " + product.getPrice()));
		
		
		//sorting
		System.out.println("*****************************");
		Collections.sort(products,(product1,product2)->{  
	        return product1.getName().compareTo(product2.getName());  
	        });
		
		 for(Product p:products){  
	            System.out.println(p.getId()+" "+p.getName()+" "+p.getPrice());  
	        }  
	}
}
