package functionalInterface;

@FunctionalInterface  
interface Print{  
    void print(String msg);
    int hashCode();  
    String toString();  
    boolean equals(Object obj);  
}  
public class FunctionalInterface1 implements Print{
	public void print(String msg) {
		System.out.println(msg);
	}
	public static void main(String[] args) {
		FunctionalInterface1 functionalInterface1 = new FunctionalInterface1();
		functionalInterface1.print("Hello");
	}

}
