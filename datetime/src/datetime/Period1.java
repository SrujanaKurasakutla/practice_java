package datetime;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.Temporal;

public class Period1 {

	public static void main(String[] args) {
		LocalDate now = LocalDate.now();
		System.out.println("now : "+now);
		Period period = Period.ofDays(24);  
	    Temporal temp = period.addTo(LocalDate.now());  
	    System.out.println("The date after 24 days from now " +temp);  
	    
	    Period period1 = Period.of(2021,7,18);
	    System.out.println(period1.toString());
	    
	    Period period2 = Period.ofMonths(4);   
	    Period period3 = period2.minus(Period.ofMonths(2));  
	    System.out.println("before period "+period3);
	    
	    Period period4 = period2.plus(Period.ofMonths(2));
	    System.out.println("after period "+period4);

	}

}
