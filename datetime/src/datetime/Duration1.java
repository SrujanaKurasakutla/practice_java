package datetime;

import java.time.Duration;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class Duration1 {

	public static void main(String[] args) {
		Duration duration1 = Duration.between(LocalTime.NOON, LocalTime.MAX);
		System.out.println(duration1.get(ChronoUnit.SECONDS));

		// System.out.println(LocalTime.NOON);
		// System.out.println(LocalTime.MAX);

		Duration duration2 = Duration.between(LocalTime.MAX, LocalTime.NOON);
		System.out.println(duration2.isNegative());
		Duration duration3 = Duration.between(LocalTime.NOON, LocalTime.MAX);
		System.out.println(duration3.isNegative());

		Duration duration4 = duration1.minus(duration1);
		System.out.println(duration4.getSeconds());
		
		Duration duration5 = duration1.plus(duration1);
		System.out.println(duration5.getSeconds());
		

	}

}
