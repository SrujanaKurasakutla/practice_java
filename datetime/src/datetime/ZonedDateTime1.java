package datetime;

import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ZonedDateTime1 {

	public static void main(String[] args) {
		LocalDateTime  ldt = LocalDateTime.of(2020, 7,  19,   10,   0);  
	    ZoneId  india = ZoneId.of("Asia/Kolkata");   
	    ZonedDateTime zone1  = ZonedDateTime.of(ldt, india);   
	    System.out.println("In India Central Time Zone: " + zone1);  
	    ZoneId  tokyo = ZoneId.of("Asia/Tokyo");   
	    ZonedDateTime zone2   = zone1.withZoneSameInstant(tokyo);   
	    System.out.println("In Tokyo Central Time Zone:"  + zone2); 
	    
	    System.out.println(zone1.getZone()); 
	    
	    ZonedDateTime minus = zone1.minus(Period.ofDays(126));  
	    System.out.println("The zoned date time before 126 from now "+minus);
	    
	    ZonedDateTime plus = zone1.plus(Period.ofDays(126));  
	    System.out.println("The zoned date time before 126 from now "+plus);

	}

}
