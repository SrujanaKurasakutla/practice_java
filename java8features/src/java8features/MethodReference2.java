package java8features;

import pojo.MyInterface1;

public class MethodReference2 {
    public void printMethod() {
    	System.out.println("Reference to instance method");
    }
	public static void main(String[] args) {
	MethodReference2 reference = new MethodReference2();
	MyInterface1 myInterface = reference::printMethod;
	myInterface.print();

	}

}
