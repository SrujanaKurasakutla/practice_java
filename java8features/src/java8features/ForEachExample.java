package java8features;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

import pojo.MyConsumer;
import pojo.Product;

public class ForEachExample {
	public static void main(String[] args) {
		List<Product> productsList = new ArrayList<Product>();
		productsList.add(new Product(1, "table", 25000f));
		productsList.add(new Product(2, "bench", 30000f));
		productsList.add(new Product(3, "chair", 8000f));
		productsList.add(new Product(4, "laptop", 28000f));
		productsList.add(new Product(5, "Apple", 9f));
		
		System.out.println("***Iterating elements using Iterator***");
		Iterator<Product> iterator = productsList.iterator();
		while(iterator.hasNext()) {
			System.out.println(iterator.next().getName());
		}
		
		System.out.println("***Iterating elements using forEach method of Iterable using anonymous class***");
		productsList.forEach(new Consumer<Product>() {

			@Override
			public void accept(Product product) {
				System.out.println(product.getName());		
			}
		});
        
		System.out.println("***Iterating with consumer Interface implementation***");
		MyConsumer consumer = new MyConsumer();
		productsList.forEach(consumer);
	}
}
