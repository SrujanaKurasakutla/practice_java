package pojo;

import java.util.function.Consumer;

public class MyConsumer implements Consumer<Product>{

	@Override
	public void accept(Product product) {
	 System.out.println(product.getName());
	}

}
