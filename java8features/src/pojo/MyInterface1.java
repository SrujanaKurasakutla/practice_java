package pojo;

@FunctionalInterface
public interface MyInterface1 {
    void print();
    default void printString(String string) {
   	 System.out.println("Interface1 default method : "+string);
   }
    
    static void say() {
    	System.out.println("Static method");
    }
    
}
