package pojo;


@FunctionalInterface
public interface MyInterface2 {
	void display();
	default void printString(String string) {
    	 System.out.println("Interface2 default method : "+string);
    }
}
