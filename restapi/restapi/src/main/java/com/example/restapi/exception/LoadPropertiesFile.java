package com.example.restapi.exception;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class LoadPropertiesFile {
	static Properties prop = new Properties();
	static InputStream input;
	public static Properties getPropertyObject() {
		try
		{
			input = new FileInputStream(
					"C:\\Users\\kurasakutla.srujana\\Downloads\\restapi\\restapi\\src\\main\\resources\\ErrorMessages.properties");
			prop.load(input);

		}catch(
		IOException execption)
		{
			// execption.printStackTrace();
			System.out.println("Properties file not found");
		}
		return prop;
	}
}
