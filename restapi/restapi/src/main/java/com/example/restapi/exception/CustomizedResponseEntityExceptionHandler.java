package com.example.restapi.exception;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	/*Properties prop = new Properties();
	InputStream input = null;*/

	@ExceptionHandler(EmployeeNotFoundException.class)
	public final ResponseEntity<ErrorResponse> handleUserNotFoundException(EmployeeNotFoundException ex,WebRequest request){
		Properties prop = LoadPropertiesFile.getPropertyObject();

		/*try
		{
			input = new FileInputStream(
					"C:\\Users\\kurasakutla.srujana\\Downloads\\restapi\\restapi\\src\\main\\resources\\ErrorMessages.properties");
			prop.load(input);

		}catch(
		IOException execption)
		{
			// execption.printStackTrace();
			System.out.println("Properties file not found");
		}*/
		ErrorResponse errorResponse = new ErrorResponse(new Date(), 4041,
				prop.getProperty("4041") + "on :: " + ex.getMessage(), request.getDescription(false));
		return ResponseEntity.status(4041).body(errorResponse);
	}
	@ExceptionHandler(InvalidEmailException.class)
	public final ResponseEntity<ErrorResponse> handleInvalidEmailException(InvalidEmailException ex,WebRequest request){
		Properties prop = LoadPropertiesFile.getPropertyObject();
		ErrorResponse errorResponse = new ErrorResponse(new Date(), 4061,
				prop.getProperty("4061") +"::"+ ex.getMessage(), request.getDescription(false));
		return ResponseEntity.status(4061).body(errorResponse);
	}

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ErrorResponse> handleAllExceptions(Exception ex, WebRequest request) {
		ErrorResponse errorDetails = new ErrorResponse(new Date(), HttpStatus.INTERNAL_SERVER_ERROR.value(),
				ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
