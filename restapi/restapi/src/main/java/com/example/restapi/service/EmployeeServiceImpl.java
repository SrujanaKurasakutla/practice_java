package com.example.restapi.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.restapi.dao.EmployeeDao;
import com.example.restapi.exception.EmployeeNotFoundException;
import com.example.restapi.exception.InvalidEmailException;
import com.example.restapi.model.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	@Autowired
	EmployeeDao employeeDao;

	@Override
	public Iterable<Employee> getAllEmployees() {
		return employeeDao.findAll();
	}

	@Override
	public ResponseEntity<Employee> getEmployeeById(Long employeeId) throws EmployeeNotFoundException {
		Employee employee = employeeDao.findById(employeeId)
				.orElseThrow(() -> new EmployeeNotFoundException("" + employeeId));
		return ResponseEntity.ok().body(employee);
	}

	@Override
	public Employee createEmployee(@Valid Employee employee) throws InvalidEmailException{
		Employee newEmployee = null;
		try {
			newEmployee = employeeDao.save(employee);
		} catch(Exception ex) {
			throw new InvalidEmailException(employee.getEmail() + "");
		}
		return newEmployee;
	}

	@Override
	public ResponseEntity<Employee> updateEmployee(@Valid Employee employee) throws EmployeeNotFoundException {
		Employee emp = employeeDao.findById(employee.getId())
				.orElseThrow(() -> new EmployeeNotFoundException("" + employee.getId()));
		emp.setFirstName(employee.getFirstName());
		emp.setLastName(employee.getLastName());
		emp.setEmail(employee.getEmail());
		final Employee updatedEmployee = employeeDao.save(emp);
		return ResponseEntity.ok(updatedEmployee);
		
		
	}

	@Override
	public Map<String, Boolean> deleteEmployee(Long employeeId) throws Exception {
		/*
		 * try { employeeDao.deleteById(employeeId); return new
		 * ResponseEntity<>(HttpStatus.NO_CONTENT); } catch (Exception e) {
		 * 
		 * }
		 */
		Employee employee = employeeDao.findById(employeeId)
				.orElseThrow(() -> new EmployeeNotFoundException("" + employeeId));
		employeeDao.delete(employee);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

}
