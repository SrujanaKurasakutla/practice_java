package com.example.restapi.controller;


import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.restapi.exception.EmployeeNotFoundException;
import com.example.restapi.exception.InvalidEmailException;
import com.example.restapi.model.Employee;
import com.example.restapi.service.EmployeeService;




@RestController
@RequestMapping("/api")
public class EmployeeController {
	@Autowired
	private EmployeeService employeeService;
	
	 @GetMapping("/employees")
	  public Iterable<Employee> getAllEmployees() {
	    return employeeService.getAllEmployees();
	  }
	 
	 @GetMapping("/employees/{id}")
	  public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") Long employeeId) throws EmployeeNotFoundException
	       {
	   return employeeService.getEmployeeById(employeeId);
	  }
	 @PostMapping("/employees")
	  public Employee createEmployee(@Valid @RequestBody Employee employee) throws InvalidEmailException {
	    return employeeService.createEmployee(employee);
	  }
	 @PutMapping("/employees")
	  public ResponseEntity<Employee> updateEmployee(@Valid @RequestBody Employee employee) throws EmployeeNotFoundException{
		 return employeeService.updateEmployee(employee);
		
	  }
	 @DeleteMapping("/employees/{id}")
	 public Map<String, Boolean> deleteEmployee(@PathVariable("id") Long employeeId) throws Exception  {
	    return employeeService.deleteEmployee(employeeId);
	  }
}
